﻿Kristo Koval ( B )
Henri Lillemaa ( B )
Vladislav Paskevits ( A )
Aap Vare ( A )

###################

Overview
The task is to have the robot remove trash from a given area of a room and take it to the marked place. The area is 4*4 meters. The trash is green paper balls. The trash destination is marked with pink edges. The camera has a bird’s-eye view of the area. The recorded area is mapped and the trash objects inside are detected using image processing. The robot starts at one of the four corners of the area. Based on the trash cleaning algorithm and the map of the area, the robot starts picking up the objects (using servos) and taking them to the designated destination. The robot stops moving once the area has been cleaned. The robot is autonomous while cleaning but gets image processing information from another computer which has the camera connected to it.

Schedule for Pair A
Week 10 - starting to write template algorithms for trash cleaning and moving the robot
Week 11 - developing template algorithms for trash cleaning and moving the robot
Week 12 - getting the algorithms to work on the hardware
Week 13 - (proof of concept on Monday) integrating our solutions with pair B
Week 14 - integrating the solutions further, making necessary adjustments, fixing bugs, etc
Week 15 - (project poster due Wednesday) finishing touches, bug fixes, etc
Week 16 - (poster session with live demo on Monday)

Schedule for Pair B

Week 11 - developing area mapping algortihm 
Week 12 - finishing area mapping and starting with robot tracking 
Week 13 - robot trackig and area mapping working together, integratiing with pair A
Week 14 - bug fixes and working prototype
Week 15 - bug fixes, finishing touches
Week 16 - presentation

Component list

* Raspberry pi 3 (x1)

* GoPIGo 2 (x1)

* Arduino Nano (x1)

* Camera (x1)

* Battery for the robot (x1)

* General servo (x2)

Challenges and solutions

* Taking the trash to the "trash bin" - the robot has to know where the destination of the trash is located, take the trash there and memorize the route. It should be possible for the robot to know where the trash destination is located but we need to implement the area mapping for it.

* Testing environment - we were unsuccesful in getting VNC Viewer to work outside of the robotics classroom. The testing area is a bit of a problem as well. 16 square meters is a pretty large area. We can test the robot in a smaller area but we have to adjust the code accordingly.

####################

import gopigo as go
import time

i = 0
k = 1

while i < 30:
    try:
        go.set_speed(40+i)
        go.set_left_speed(100-i)
        go.fwd()
        time.sleep(5*k)
        i += 1
        k += 1
    except KeyboardInterrupt:
        break
go.stop()

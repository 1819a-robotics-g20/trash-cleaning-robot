# http://www.robotc.net/wikiarchive/Tutorials/Arduino_Projects/Mobile_Robotics/VEX/Using_encoders_to_drive_straight
import gopigo as go
import time
# The powers we give to both motors. masterPower will remain constant while slavePower will change so that
# the right wheel keeps the same speed as the left wheel.
masterPower = 40
slavePower = 40
 
# Essentially the difference between the master encoder and the slave encoder. Negative if slave has 
# to slow down, positive if it has to speed up. If the motors moved at exactly the same speed, this
# value would be 0.
error = 0
 
# 'Constant of proportionality' which the error is divided by. Usually this is a number between 1 and 0 the
# error is multiplied by, but we cannot use floating point numbers. Basically, it lets us choose how much 
# the difference in encoder values effects the final power change to the motor.
kp = 5
 
# Reset the encoders.
#võtab nullkohad
enc0_l = go.enc_read(0)
enc0_r = go.enc_read(1)
enc_l = go.enc_read(0) - enc0_l
enc_r = go.enc_read(1) - enc0_r
 
# Repeat ten times a second.
while True:
    # Set the motor powers to their respective variables.
    go.set_left_speed(masterPower)
    go.set_right_speed(slavePower)
 
    # This is where the magic happens. The error value is set as a scaled value representing the amount the slave
    # motor power needs to change. For example, if the left motor is moving faster than the right, then this will come
    # out as a positive number, meaning the right motor has to speed up.
    error = enc_l - enc_r
 
    # This adds the error to slavePower, divided by kp. The '+=' operator literally means that this expression really says 
    # "slavePower = slavepower + error / kp", effectively adding on the value after the operator.
    # Dividing by kp means that the error is scaled accordingly so that the motor value does not change too much or too 
    # little. You should 'tune' kp to get the best value. For us, this turned out to be around 5. 
    slavePower += error / kp
 
    # Reset the encoders every loop so we have a fresh value to use to calculate the error.
    enc0_l = go.enc_read(0)
    enc0_r = go.enc_read(1)
    enc_l = go.enc_read(0) - enc0_l
    enc_r = go.enc_read(1) - enc0_r
 
    # Makes the loop repeat ten times a second. If it repeats too much we lose accuracy due to the fact that we don't have
    # access to floating point math, however if it repeats to little the proportional algorithm will not be as effective.
    # Keep in mind that if this value is changed, kp must change accordingly.
    time.sleep(0.1)
  }
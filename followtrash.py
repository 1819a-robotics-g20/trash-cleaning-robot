# -*- coding: utf-8 -*-

import cv2
import gopigo as go
import numpy as np
import time

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.minArea = 100
blobparams.maxArea = 100000
blobparams.filterByCircularity = False
blobparams.minDistBetweenBlobs = 10
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False
detector = cv2.SimpleBlobDetector_create(blobparams)

# global variable for determining gopigo speed
gospeed = 40

# global variable for video feed
cap = None

def init():
    global cap, gospeed
    # This function should do everything required to initialize the robot.
    # Among other things it should open the camera and set gopigo speed.
    # Some of this has already been filled in.
    # You are welcome to add your own code, if needed.

    cap = cv2.VideoCapture(0)
    go.set_speed(gospeed)
    return

# TASK 1
def get_line_location(frame, lH, lS, lV, hH, hS, hV):
    # This function should use frame from camera to determine line location.
    # It should return location of the line in the frame.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    lB = lH
    lG = lS
    lR = lV
    hB = hH
    hG = hS
    hR = hV
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    # Our operations on the frame come here
    thresholded = cv2.inRange(hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(hsv, hsv, mask = thresholded)

    k = np.nonzero(thresholded)
    k = np.mean(k[1])

    keypoints = detector.detect(thresholded)
    for keypoint in keypoints:
        x = keypoint.pt[0]
        y = keypoint.pt[1]
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imshow('Processed', thresholded)
    #print(k)
    return k
    
# default movement algorithm goes here
def default():
    go.set_speed(40)
    go.set_left_speed(100)
    go.fwd()

enc01 = 0
enc02 = 0
dist = 0
def bang_bang_with_hysteresis(linelocation):
    # This function should use the line location to implement bang-bang controller with hysteresis.
    # YOUR CODE HERE
    global enc01
    global enc02
    global dist
    go.set_speed(40)
    if linelocation > 400:
        go.set_right_speed(40)
        go.set_left_speed(70)
        go.fwd()
    elif linelocation < 200:
        go.set_right_speed(70)
        go.set_left_speed(40)
        go.fwd()
    elif dist < 50 and linelocation >= 200 and linelocation <= 400:
        #enkoodrid hakkavad siit lugema
        if enc01 == 0:
            enc01 = go.enc_read(0)
        if enc02 == 0:
            enc02 = go.enc_read(1)
        enc1 = go.enc_read(0) - enc01
        enc2 = go.enc_read(1) - enc02
        dist = int((enc1 + enc2) / 2)
        go.fwd()
        go.set_right_speed(50)
        go.set_left_speed(50)
        if dist >= 50:
            enc01 == 0
            enc02 == 0
        print(dist)
    elif dist >= 100:
        enc01 = 0
        enc02 = 0
        dist = 0
    else:
        go.stop()
    return

# Initialization
init()

fps = 0
str_fps = " "
seconds = 0
blue = 0

while True:
    # start = time.time()
    # We read information from camera.
    ret, frame = cap.read()
    r = [len(frame)-200, len(frame)-170, 0, len(frame[0])]
    frame = frame[r[0]:r[1], r[2]:r[3]]
    # cv2.putText(frame, str_fps, (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    
    if blue == 0:
        linelocation = get_line_location(frame, 33, 55, 55, 70, 200, 240)
        blue = 1
    else:
        linelocation = get_line_location(frame, 45, 45, 110, 155, 255, 255)
        blue = 0
    if linelocation == linelocation:
        bang_bang_with_hysteresis(linelocation)
    else:
        if dist >= 50 and dist < 100:
            enc1 = go.enc_read(0) - enc01
            enc2 = go.enc_read(1) - enc02
            dist = int((enc1 + enc2) / 2)
            go.bwd()
            go.set_right_speed(50)
            go.set_left_speed(50)
            print(dist)
        else:
            default()
    cv2.imshow('Original', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # end = time.time()
    # seconds += end - start
    # if seconds < 1:
    #     fps += 1
    # elif seconds >= 1:
    #     str_fps = str(int(round(fps)))
    #     seconds = 0
    #     fps = 0

cap.release()
cv2.destroyAllWindows()
go.stop()

# -*- coding: utf-8 -*-
# TUVASTAB AINULT ROHELIST
# PÄRAST ROHELISE ÄRA VEDAMIST JÄÄB SEISMA KUNI PALL EEST ÄRA VÕETAKSE

import cv2
import gopigo as go
import numpy as np
import time

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.minArea = 100
blobparams.maxArea = 100000
blobparams.filterByCircularity = False
blobparams.minDistBetweenBlobs = 10
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False
detector = cv2.SimpleBlobDetector_create(blobparams)

# global variable for determining gopigo speed
gospeed = 40

# global variable for video feed
cap = None

def init():
    global cap, gospeed
    # This function should do everything required to initialize the robot.
    # Among other things it should open the camera and set gopigo speed.
    # Some of this has already been filled in.
    # You are welcome to add your own code, if needed.

    cap = cv2.VideoCapture(0)
    go.set_speed(gospeed)
    return

# TASK 1
def get_line_location(frame, lH, lS, lV, hH, hS, hV):
    # This function should use frame from camera to determine line location.
    # It should return location of the line in the frame.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    lB = lH
    lG = lS
    lR = lV
    hB = hH
    hG = hS
    hR = hV
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    # Our operations on the frame come here
    thresholded = cv2.inRange(hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(hsv, hsv, mask = thresholded)

    k = np.nonzero(thresholded)
    k = np.mean(k[1])

    keypoints = detector.detect(thresholded)
    for keypoint in keypoints:
        x = keypoint.pt[0]
        y = keypoint.pt[1]
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imshow('Processed', thresholded)
    #print(k)
    return k
    
# default movement algorithm goes here
def default():
    #go.set_speed(40)
    #go.set_left_speed(100)
    #go.fwd()
    global i
    global j
    global seconds
    global pstop
    start = int(time.time())
    
    if seconds >= 0 and seconds < 40:
        go.set_speed(40+i)
        go.set_left_speed(150-i)
        go.fwd()
        if seconds == 10:
            i = 10
        elif seconds == 20:
            i = 20
        elif seconds == 30:
            i = 30
    elif seconds >= 40 and seconds < 100:
        go.set_speed(75)
        go.set_left_speed(115)
        go.fwd()
    if seconds == 100:
        go.stop()
        pstop = 1
    end = int(time.time())
    seconds += end - start
    #print(seconds)

enc01 = 0
enc02 = 0
dist = 0
color = -1
def bang_bang_with_hysteresis(linelocation):
    # This function should use the line location to implement bang-bang controller with hysteresis.
    # YOUR CODE HERE
    global color
    global enc01
    global enc02
    global dist
    go.set_speed(40)
    #if color == -1:
    #    color = colors
    if linelocation > 400:
        go.set_right_speed(40)
        go.set_left_speed(70)
        go.fwd()
    elif linelocation < 200:
        go.set_right_speed(70)
        go.set_left_speed(40)
        go.fwd()

    elif color == 0 and dist < 50 and linelocation >= 200 and linelocation <= 400:
        #enkoodrid hakkavad siit lugema
        if enc01 == 0:
            enc01 = go.enc_read(0)
        if enc02 == 0:
            enc02 = go.enc_read(1)
        enc1 = go.enc_read(0) - enc01
        enc2 = go.enc_read(1) - enc02
        dist = int((enc1 + enc2) / 2)
        go.fwd()
        go.set_right_speed(50)
        go.set_left_speed(50)
        if dist >= 50:
            enc01 == 0
            enc02 == 0
        print(dist, color)
    elif color == 0 and dist >= 100:
        enc01 = 0
        enc02 = 0
        dist = 0

    elif color == 1 and dist < 70 and linelocation >= 200 and linelocation <= 400:
        #enkoodrid hakkavad siit lugema
        if enc01 == 0:
            enc01 = go.enc_read(0)
        if enc02 == 0:
            enc02 = go.enc_read(1)
        enc1 = go.enc_read(0) - enc01
        enc2 = go.enc_read(1) - enc02
        dist = int((enc1 + enc2) / 2)
        go.fwd()
        go.set_right_speed(50)
        go.set_left_speed(50)
        if dist >= 70:
            enc01 == 0
            enc02 == 0
        print(dist, color)
    elif color == 1 and dist >= 120:
        enc01 = 0
        enc02 = 0
        dist = 0
        
    else:
        go.stop()
    return

# Initialization
init()

i = 0
j = 0
#fps = 0
#str_fps = " "
seconds = 0
blue = 0
dist == 0
color = -1
pstop = 0

while True:
    # We read information from camera.
    ret, frame = cap.read()
    r = [len(frame)-200, len(frame)-170, 0, len(frame[0])]
    frame = frame[r[0]:r[1], r[2]:r[3]]
    # cv2.putText(frame, str_fps, (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    
    if blue == 0:
        linelocation = get_line_location(frame, 33, 70, 55, 70, 200, 240)
        blue = 1
        #colors = 0
    else:
        linelocation = get_line_location(frame, 45, 70, 110, 155, 255, 255)
        blue = 0
        #colors = 1
    if linelocation == linelocation:
        if color == -1:
            color = blue
        bang_bang_with_hysteresis(linelocation)
        #if colors == -1:
        #    colors = blue
        if color == 0 and dist >= 50 and dist < 100:
            enc1 = go.enc_read(0) - enc01
            enc2 = go.enc_read(1) - enc02
            dist = int((enc1 + enc2) / 2)
            go.bwd()
            go.set_right_speed(50)
            go.set_left_speed(50)
            print(dist, color)
            
        elif color == 1 and dist >= 70 and dist < 120:
            enc1 = go.enc_read(0) - enc01
            enc2 = go.enc_read(1) - enc02
            dist = int((enc1 + enc2) / 2)
            go.bwd()
            go.set_right_speed(50)
            go.set_left_speed(50)
            print(dist, color)
            
    if color == 0 and dist >= 50 and dist < 100:
        enc1 = go.enc_read(0) - enc01
        enc2 = go.enc_read(1) - enc02
        dist = int((enc1 + enc2) / 2)
        go.bwd()
        go.set_right_speed(50)
        go.set_left_speed(50)
        print(dist, color)
            
    elif color == 1 and dist >= 70 and dist < 120:
        enc1 = go.enc_read(0) - enc01
        enc2 = go.enc_read(1) - enc02
        dist = int((enc1 + enc2) / 2)
        go.bwd()
        go.set_right_speed(50)
        go.set_left_speed(50)
        print(dist, color)
    else:
        default()
    if dist == 0:
        color = -1
    if pstop == 1:
        break
    cv2.imshow('Original', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
go.stop()

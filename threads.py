#!/usr/bin/env python3
# coding=utf-8
import serial
import gopigo as go
import time
import cv2
import json
import os
import time
import numpy as np
import _thread

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.minArea = 100
blobparams.maxArea = 100000
blobparams.filterByCircularity = False
blobparams.minDistBetweenBlobs = 10
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False
detector = cv2.SimpleBlobDetector_create(blobparams)

running = True

# global variable for determining gopigo speed
gospeed = 40

# global variable for video feed
cap = None

def init():
    global cap, gospeed
    # This function should do everything required to initialize the robot.
    # Among other things it should open the camera and set gopigo speed.
    # Some of this has already been filled in.
    # You are welcome to add your own code, if needed.

    cap = cv2.VideoCapture(0)
    go.set_speed(gospeed)
    return

def get_line_location(frame):
    # This function should use frame from camera to determine line location.
    # It should return location of the line in the frame.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    lB = 40
    lG = 55
    lR = 55
    hB = 70
    hG = 100
    hR = 240
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    # Our operations on the frame come here
    thresholded = cv2.inRange(hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(hsv, hsv, mask = thresholded)

    k = np.nonzero(thresholded)
    k = np.mean(k[1])

    keypoints = detector.detect(thresholded)
    for keypoint in keypoints:
        x = keypoint.pt[0]
        y = keypoint.pt[1]
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imshow('Processed', thresholded)
    print(k)
    return k

def bang_bang_with_hysteresis(linelocation):
    # This function should use the line location to implement bang-bang controller with hysteresis.
    # YOUR CODE HERE
    if linelocation > 350:
        go.set_right_speed(50)
        go.set_left_speed(80)
        go.fwd()
    elif linelocation < 290:
        go.set_right_speed(80)
        go.set_left_speed(50)
        go.fwd()
    elif linelocation >= 290 and linelocation <= 350:
        go.fwd()
    else:
        go.stop()
    return

'''
A function to run in a separate thread from the line sensor. Since we want to
read the line sensors as quickly as possible then we would want to run slower
operations such as image processing here. This function will run in a separate thread
as long as the 'running' variable is True. This variable is only set to false
when the main thread is stopped.
'''
def slowThread():
    global us_pos
    global enc_pos
    global cam_pos
    global scale
    global running
    
    init()
    while running:
        # Slower code goes here
        # We read information from camera.
        ret, frame = cap.read()
        r = [len(frame)-200, len(frame)-120, 0, len(frame[0])]
        frame = frame[r[0]:r[1], r[2]:r[3]]
        #cv2.putText(frame, str_fps, (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

        linelocation = get_line_location(frame)
        bang_bang_with_hysteresis(linelocation)
        cv2.imshow('Original', frame)

# Draws positions read from different sources on the map and then displays it.
def drawMap(us_pos=0, enc_pos=0, cam_pos=0, scale=0.5):
    img = np.copy(img_orig)

    bluePos = int(-0.869565217 * cam_pos + 1478)   # camera
    greenPos = int(-0.869565217 * us_pos + 1478)    # ultrasonic
    redPos = int(-0.869565217 * enc_pos + 1478)   # encoders

    cv2.circle(img, (redPos,  100), int(15/scale), (0,0,255), -1)
    cv2.circle(img, (greenPos,180), int(15/scale), (0,255,0), -1)
    cv2.circle(img, (bluePos, 260), int(15/scale), (255,0,0), -1)
    cv2.putText(img, "Enc", (redPos, 100 - 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 2)
    cv2.putText(img, "US",  (greenPos, 180 - 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
    cv2.putText(img, "Cam", (bluePos, 260), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2)
    img = cv2.resize(img, (0,0), fx=scale, fy=scale)
    cv2.imshow('map',img)
    cv2.waitKey(1)


lineSensorOffset = 0
try:
    _thread.start_new_thread(slowThread, ()) # Start the second thread.
    go.set_speed(70)

    '''
    This is the main thread, which should be running more important code such as
    Getting the sensor info from the serial and driving the robot.
    '''
    while True:
        
        # Line following logic goes here
        go.forward()
        go.set_left_speed(71)
        time.sleep(30)
        go.stop()
        go.left()
        time.sleep(2.1)
        go.stop()
        go.fwd()
        time.sleep(3.5)
        go.stop()
        go.left()
        time.sleep(2.1)
        go.stop()
        go.fwd()
        go.set_left_speed(71)
        time.sleep(30)
        go.stop()

except KeyboardInterrupt:
    print("Serial closed, program finished")

finally:
    running = False # Stop other threads.
go.stop()